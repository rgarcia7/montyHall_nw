const Sequelize = require('sequelize');

function sleep(ms){
    return new Promise(resolve=>{
        setTimeout(resolve,ms)
    })
}
async function fixPostgres(){
   console.log("IntentandoConectar...");
   conectarABase();
   await sleep(100);
   console.log("Reintentando...");   
   //window.location.reload();
}

function conectarABase(){
	window.sequelize = new Sequelize('historial', 'postgres', null, {
	  host: 'localhost',
	  dialect: 'postgres',
	  define: {
	        timestamps: false
    },
	  pool: {
	    max: 5,
	    min: 0,
	    idle: 10000
	  }
	});
	sequelize
	  .authenticate()
	  .then(() => {
	    console.log('Connection has been established successfully.');
	    var audio = document.getElementsByTagName("audio")[0];
		audio.play();
		keepChecking=false;
	  })
	  .catch(err => {
	    console.error('Unable to connect to the database:', err);
	    var spawn = require('child_process').spawn,
		    child    = spawn('..\\pgsql\\PostgreSQLPortable.exe');
		
	   	
		child.stdout.on('data', function (data) {
		  console.log('stdout: ' + data);
		});

		child.stderr.on('data', function (data) {
		  console.log('stderr: ' + data);
		});

		child.on('close', function (code) {
		  console.log('child process exited with code ' + code);
		});

		fixPostgres();
	  });
	//Modelos
	window.Puntaje = window.sequelize.define('puntaje', {
	  numeroJuego: {
	    type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
	  },
	  eleccion: {
	    type: Sequelize.STRING
	  },
	  resultado: {
	    type: Sequelize.STRING
	  }
	});
	window.tablaPuntajes = Puntaje
	  .findAndCountAll({
	  	order: sequelize.col('numeroJuego'),
	  	raw: true
	  })
	  .then(result => {
	    console.log(result.count);
	    window.myRecordsOri = result.rows;	//JSON.stringify(result.rows);
		 $('#example').dataTable({
		 	"paging": true,
		 	"scrollY": 164,
		    "deferRender": true,
		    "scroller": true,
		    "data": myRecordsOri,
		    "columns": [
		        {title: "Numero Juego" , "mDataProp": "numeroJuego" },
		        {title: "Eleccion", "mDataProp": "eleccion" },
		        {title: "Resultado", "mDataProp": "resultado" }
		    ],
		    "oLanguage": {
			  "sProcessing":     "Procesando...",
			    "sLengthMenu":     "Mostrar _MENU_ registros",
			    "sZeroRecords":    "No se encontraron resultados",
			    "sEmptyTable":     "Ningun dato disponible en esta tabla",
			    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			    "sInfoPostFix":    "",
			    "sSearch":         "Buscar:",
			    "sUrl":            "",
			    "sInfoThousands":  ",",
			    "sLoadingRecords": "Cargando...",
			    "oPaginate": {
			        "sFirst":    "Primero",
			        "sLast":     "Último",
			        "sNext":     "Siguiente",
			        "sPrevious": "Anterior"
			    },
			    "oAria": {
			        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			    }
			}
		});	
	  });
}

$( document ).ready(function() {
	fixPostgres();
});

function recargarDatosenTabla(){
		Puntaje
		  .findAndCountAll({
		  	order: sequelize.col('numeroJuego'),
		  	raw: true
		  })
		  .then(result => {
		    console.log(result.count);
		    window.myRecordsOri = result.rows;	//JSON.stringify(result.rows);
	    	$('#example').DataTable().clear().rows.add(myRecordsOri).draw()
		  });
}


function almacenarResultado(eleccionVar,resultadoVar){
	//Modelos
	const Puntaje = window.sequelize.define('puntaje', {
	  numeroJuego: {
	    type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
	  },
	  eleccion: {
	    type: Sequelize.STRING
	  },
	  resultado: {
	    type: Sequelize.STRING
	  }
	});
	Puntaje.sync({force: false}).then(() => {
	  // Table created
	  return Puntaje.create({
	    eleccion: eleccionVar,
	    resultado: resultadoVar
	  });
	}).then(() => {
	  recargarDatosenTabla();
  	});
}

function borrarBaseDatos(){
	Puntaje.destroy({
		where: {},
		truncate: true,
		restartIdentity: true
	}).then(function () {
		window.location.reload();
	});	
}