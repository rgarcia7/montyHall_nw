const win = nw.Window.get();

window.addEventListener("wheel", function (e) {
    if (e.ctrlKey) {
        if (e.deltaY > 0) {
            win.zoomLevel -= 0.5
        }
        else {
            win.zoomLevel += 0.5;
        }
    }
});