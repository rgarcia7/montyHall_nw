var random = require("random-js")(); // uses the nativeMath engine
var puertaConPremio = random.integer(1, 3);
var firstSelectedDoor = -1;
var indexSelectedDoor = -1;
var baseMargin = 368;
var clicCount = 0;
var abrirVacia = -1;

function openAllDoors(){//Abrir todas las puertas
	$(".redDoor").each(function(){
	    $(this).css('background-image', "url('../app/img/openDoorRed.png')");
	    $(this).attr('onclick','').unbind('click');
	});
	$(".blackDoor").each(function(){
	    $(this).css('background-image', "url('../app/img/openDoorBlack.png')");
	    $(this).attr('onclick','').unbind('click');
	});
}

function mostrarCarro(){
	var increaseMargin = 0;
	switch ( puertaConPremio ) {
	    case 1:
	        textPuerta = "puerta1";
	        increaseMargin = baseMargin;
	    break;
	    case 2:
	        textPuerta = "puerta2";
	        increaseMargin = baseMargin + 197;
	    break;
	    default:
	        textPuerta = "puerta3";
	        increaseMargin = baseMargin + 393;
	    break;
	}
	var puertaBlack = $("#" + textPuerta + "Black");
	var puertaRed = $("#" + textPuerta + "Red");
	console.log(puertaBlack);
	//puertaBlack.css('background-image', "url('../app/img/closedDoorBlack.png'),url('../app/img/camaro.jpg')");
	puertaBlack.append($('<div style="left:' + increaseMargin +'px;top:90px;z-index: -1;background-size: 145px;width:145px;height:258px;position:absolute;display:inline-block;background-image:url(../app/img/camaro.png)";></div>'));
	puertaRed.append($('<div style="left:' + increaseMargin +'px;top:90px;z-index: -1;background-size: 145px;width:145px;height:258px;position:absolute;display:inline-block;background-image:url(../app/img/camaro.png)";></div>'));
	openAllDoors();
}

function escogerPuerta(puerta,color) {
	let textPuerta = '';
		switch ( puerta ) {
		    case 'puerta1':
		    	indexSelectedDoor = 1;
		        textPuerta = "puerta 1";
		    break;
		    case 'puerta2':
		    	indexSelectedDoor = 2;
		        textPuerta = "puerta 2";
		    break;
		    default:
		    	indexSelectedDoor = 3;
		        textPuerta = "puerta 3";
		    break;
		}
	if(clicCount ===0 ){
		firstSelectedDoor=indexSelectedDoor;
		var puertaBlack = document.getElementById(puerta + "Black");
		var puertaRed= document.getElementById(puerta + "Red");
		puertaBlack.style.display ="none";
		puertaRed.style.display ="inline-block";
		$('#selectedDoor').append("<br> Primera puerta seleccionada: " + textPuerta);
		$('#celda' + indexSelectedDoor).text("Primera eleccion");
		$('#celda' + indexSelectedDoor).css("color","yellow");
		openEmpty();
		clicCount++;
	}else{
		$('.celdasG').text("Vacia");
		$('#selectedDoor').append("<br>Ultima Puerta Seleccionada: " + textPuerta);
		$('#celda' + indexSelectedDoor).text("Ultima eleccion");
		$('#celda' + indexSelectedDoor).css("color","yellow");
		$('#selectedDoor').append("<br>Puerta Ganadora: " + puertaConPremio);
		$('#celdaG' + puertaConPremio).text("Puerta ganadora");
		$('#celdaG' + puertaConPremio).css("color","green");
		let stringEleccion='Si Cambio';
		let stringResultado='Pierde';
		if(indexSelectedDoor === firstSelectedDoor){
			stringEleccion = 'No Cambio'
		}
		if(indexSelectedDoor === puertaConPremio){
			stringResultado = 'Gana'
		}
		almacenarResultado(stringEleccion,stringResultado);
		mostrarCarro();
	}
}

function restartGame() {
	window.location.reload();
}

function openEmpty(){	
	do {
	    abrirVacia = random.integer(1, 3);
	}
	while (abrirVacia === puertaConPremio || abrirVacia === indexSelectedDoor); 
	let textPuerta = '';
	switch ( abrirVacia ) {
	    case 1:
	        textPuerta = "puerta1";
	    break;
	    case 2:
	        textPuerta = "puerta2";
	    break;
	    default:
	        textPuerta = "puerta3";
	    break;
	}
	var puertaBlack = $("#" + textPuerta + "Black");
	puertaBlack.css('background-image', "url('../app/img/openDoorBlack.png')");
	puertaBlack.attr('onclick','').unbind('click');
	$('#selectedDoor').append("<br>La puerta " + abrirVacia + " esta vacia");
	$('#celdaG' + abrirVacia).text("Vacia");
	$('#celda' + abrirVacia).text("Abierta por Anfitrion");
	$('#celda' + abrirVacia).css("color","red");
	$('#selectedDoor').append(", desea cambiar de puerta?");
}